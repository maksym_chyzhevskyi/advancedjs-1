/* Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
Об'єкти можуть отримувати один від одного певні властивості. Наприклад: методи, змінні.

Для чого потрібно викликати super() у конструкторі класу-нащадка?
Для виклику функцій методів на батьківському об'єкті.  
*/

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }
    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary
    }

    set salary(value) {
        this._salary = value;
    }

}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this._lang = lang;
    }

    get salary() {
        return (this._salary * 3)
    }
}

const programmerOne = new Programmer('Mark', 28, 2100, 'Java');
const programmerTwo = new Programmer('Lars', 25, 1900, 'C#');
const programmerThree = new Programmer('Ger', 34, 3300, 'PHP');

console.log(programmerOne, programmerOne.salary);
console.log(programmerTwo, programmerTwo.salary);
console.log(programmerThree, programmerThree.salary);












